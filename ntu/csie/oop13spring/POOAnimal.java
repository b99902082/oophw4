package ntu.csie.oop13spring;
import java.util.*;

public abstract class POOAnimal extends POOPet{
	
	protected int x;
	protected int y;
	protected boolean life;
	Random dice = new Random();
	
	public int getx(){
		return x;
	}
	public int gety(){
		return y;
	}
	public boolean alive(){
		return life;
	}
	
	public POOAnimal(){
		x = dice.nextInt(30);
		y = dice.nextInt(30);
		life = true;
	}

	public boolean cutHP(int v){
		int HP = super.getHP();
		if(HP > v) return super.setHP(HP-v);
		else Die();
		return true;
	}
	public int gHP(){
		return super.getHP();
	}
	public int gMP(){
		return super.getMP();
	}
	public int gAGI(){
		return super.getAGI();
	}
	public boolean cutMP(int v){
		int MP = super.getMP();
		if(MP > v) return super.setMP(MP-v);
		else super.setMP(0);
		return true;
	}
	public boolean cutAGI(int v){
		int AGI = super.getAGI();
		if(AGI > v) return super.setAGI(AGI-v);
		else super.setAGI(0);
		return true;
	}
	public void HPhalf(){
		int HP = super.getHP();
		super.setHP(HP/2);
		if(HP/2==0) Die();
	}
	protected void Die(){
		life = false;
	}
	
	protected int dissqr(POOAnimal p){
		return (p.getx()-x)*(p.getx()-x)+(p.gety()-y)*(p.gety()-y);
	}

	public abstract POOAction act(POOArena arena);
	public abstract POOCoordinate move(POOArena arena);
}


