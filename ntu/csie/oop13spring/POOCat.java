package ntu.csie.oop13spring;

public class POOCat extends POOAnimal{
	public POOCat(){
		setHP(800);
		setMP(500);
		setAGI(6);
		setName("Cat");
	}
	public POOAction act(POOArena arena){
		POOPet[] Pets = arena.getAllPets();
		for(int i=0; i<Pets.length; ++i)
			if(dissqr((POOAnimal)Pets[i])<=2)
				new LittleAttack().act(Pets[i]);
		return null;
	}
	public POOCoordinate move(POOArena arena){
		POOPet[] Pets = arena.getAllPets();
		for(int i=0; i<Pets.length; ++i){
			if(dissqr((POOAnimal)Pets[i])<=getAGI()){
				x = ((POOAnimal)Pets[i]).getx();
				y = ((POOAnimal)Pets[i]).gety();
				break;
			}
		}
		return null;
	}
}
