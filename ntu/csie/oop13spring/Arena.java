package ntu.csie.oop13spring;

public class Arena extends POOArena{
	public boolean fight(){
		POOPet[] Pets = super.getAllPets();
		int newmove = 0;
		for(int i=0; i<Pets.length; ++i){
			if(((POOAnimal)Pets[i]).alive()){
				Pets[i].move(this);
				Pets[i].act(this);
				newmove++;
			}
		}
		if(newmove<2) return false;
		return true;
	}
	public void show(){	
		POOPet[] Pets = super.getAllPets();
		for(int i=0; i<Pets.length; ++i){
			POOAnimal cur = (POOAnimal)Pets[i];
			int HP = cur.gHP();
			int MP = cur.gMP();
			int AGI = cur.gAGI();
			int x = cur.getx();
			int y = cur.gety();
			System.out.println(cur.getName());
			System.out.println("     ("+x+","+y+") HP="+HP+" MP="+MP+" AGI="+AGI);
		}
	}
	public POOCoordinate getPosition(POOPet p){
		return null;
	}
}
