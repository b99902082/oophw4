package ntu.csie.oop13spring;

public class POODog extends POOAnimal{
	public POODog(){
		setHP(400);
		setMP(800);
		setAGI(5);
		setName("Dog");
	}
	public POOAction act(POOArena arena){
		POOPet[] Pets = arena.getAllPets();
		for(int i=0; i<Pets.length; ++i)
			if(((POOAnimal)Pets[i]).getx()==x)
				new SuperAttack().act(Pets[i]);
		return null;
	}
	public POOCoordinate move(POOArena arena){
		POOPet[] Pets = arena.getAllPets();
		for(int i=0; i<Pets.length; ++i){
			if(dissqr((POOAnimal)Pets[i])<=getAGI()){
				x = ((POOAnimal)Pets[i]).getx();
				y = ((POOAnimal)Pets[i]).gety();
				break;
			}
		}
		return null;
	}
}
