package ntu.csie.oop13spring;

class SuperAttack extends POOSkill{
	public void act(POOPet pet){
		POOAnimal p = (POOAnimal)pet;
		p.HPhalf();
		p.cutMP(100);
		p.cutAGI(1);
	}
}

class LittleAttack extends POOSkill{
	public void act(POOPet pet){
		POOAnimal p = (POOAnimal)pet;
		p.cutHP(50);
		p.cutMP(20);
	}
}
